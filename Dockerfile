FROM alpine:edge

RUN apk add --update --no-cache nmap

ENTRYPOINT ["nmap"]